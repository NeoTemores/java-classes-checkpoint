
package com.galvanize;

public class CallingCard {
    private final int centsPerMinToUse;
    protected int remainingMinutes;

    //Constructor:
    public CallingCard(int centsPerMinToUse){
        //takes in cents and assigns it to centsPerMinToUse field
        this.centsPerMinToUse = centsPerMinToUse;
    }
    //add money:
    public void addDollars(int dollars){
        //takes in dollars and converts/assigns to remaining min
        this.remainingMinutes = (dollars * 100) / centsPerMinToUse ;

    }
    public int getRemainingMinutes(){
        System.out.println(remainingMinutes);
        return remainingMinutes;
    }

    public int useMinutes(int minutesUsed){
        remainingMinutes -= minutesUsed;
        if (remainingMinutes < 0) remainingMinutes = 0;
        return remainingMinutes;
    }

}
