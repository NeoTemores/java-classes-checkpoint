
package com.galvanize;

import java.util.ArrayList;

public class CellPhone {

    private CallingCard callingCard;
    private boolean talking = false;

    private String currentPhoneNum;
    private int callDuration = 0;

    private boolean callCutOff = false;

    private ArrayList<String> history = new ArrayList<>();

    public CellPhone(CallingCard callingCard){
        this.callingCard = callingCard;
    }

    public boolean isTalking(){
        System.out.println(talking);
        return talking;
    }

    public void call(String phoneNumber){
        talking = true;
        currentPhoneNum = phoneNumber;
    }
    public void tick(){
        callDuration++;
        callingCard.useMinutes(1);

        if(callingCard.remainingMinutes <= 0){
            callCutOff = true;
            endCall();
        }

    }
    public void endCall(){
        setHistory();
        talking = false;
        currentPhoneNum = "";
        callDuration = 0;

    }

    public void setHistory(){
        String minute = callDuration > 1 ? "minutes" : "minute";
//        callingCard.useMinutes(callDuration);
        if (callCutOff) {
            history.add(String.format("%s (cut off at %s %s)", currentPhoneNum, callDuration, minute));
        } else {
            history.add(String.format("%s (%s %s)", currentPhoneNum, callDuration, minute));

        }
    }

    public ArrayList<String> getHistory(){
        System.out.println(history);
        return history;
    }

}
